import * as React from 'react'
import {Container, Row, Col} from 'reactstrap'

import {Person} from './entities/Person'
import {PersonTableData} from './entities/PersonTableData'
import {PersonFormData} from './entities/PersonFormData'

import {PersonForm} from './PersonForm'
import {PersonTableActions} from './PersonTableActions'
import {PersonTable} from './PersonTable'

interface State {
    personTables: PersonTableData[]
    form: PersonFormData
    editingPersonTableIdx: number
    editingPersonIdx: number
}

export class HomePage extends React.Component {
    state: State = {
        personTables: [],
        form: new PersonFormData(null),
        editingPersonTableIdx: null,
        editingPersonIdx: null
    }

    componentDidMount () {
        let person = new Person({
            name: "Name",
            surname: "Surname",
            age: "Age",
            city: "City"
        })

        let personTable = new PersonTableData({
            persons: [person]
        })

        this.setState({
            personTables: [personTable]
        })
    }

    onSubmit (e) {
        e.preventDefault()

        let form = this.state.form

        if (!form.validate()) {
            this.setState({
                form: form
            })

            return false

        } else {
            if (this.state.editingPersonTableIdx != null && this.state.editingPersonIdx != null) {
                this.editPerson(form)
            } else {
                this.addPerson(form)
            }

            form.clearFields()

            this.setState({
                form: form
            })
        }

        return true
    }

    onInputChange (e) {
        let form = this.state.form

        form[e.currentTarget.name] = e.currentTarget.value

        this.setState({
            form: form
        })
    }

    addPerson (form: PersonFormData) {
        let personTables = this.state.personTables

        personTables[0].persons.splice(personTables[0].persons.length, 0, new Person(form))

        this.setState({
            personTables: personTables
        })
    }

    editPerson (form: PersonFormData) {
        let personTables = this.state.personTables
        let editingPersonTable = personTables[this.state.editingPersonTableIdx]
        let editingPerson = editingPersonTable.persons[this.state.editingPersonIdx]

        editingPerson = new Person(form)
        editingPersonTable.persons[this.state.editingPersonIdx] = editingPerson
        personTables[this.state.editingPersonTableIdx] = editingPersonTable

        this.setState({
            personTables: personTables,
            editingPersonTableIdx: null,
            editingPersonIdx: null
        })
    }

    onEditPersonClick (table_idx: number, person_idx: number) {
        let editingPerson = this.state.personTables[table_idx].persons[person_idx]

        let form = new PersonFormData(editingPerson)

        this.setState({
            form: form,
            editingPersonTableIdx: table_idx,
            editingPersonIdx: person_idx,
        })
    }

    onDeletePersonClick (table_idx: number, person_idx: number) {
        let personTables = this.state.personTables
        let persons = personTables[table_idx].persons

        persons.splice(person_idx, 1)
        personTables[table_idx].persons = persons

        this.setState({
            personTables: personTables
        })
    }

    onDeleteTableClick (table_idx: number) {
        let personTables = this.state.personTables

        personTables.splice(table_idx, 1)

        this.setState({
            personTables: personTables
        })
    }

    onCopyTableClick (table_idx: number) {
        let personTables = this.state.personTables

        personTables.splice(table_idx + 1, 0, new PersonTableData(personTables[table_idx]))

        this.setState({
            personTables: personTables
        })
    }

    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col sm={{ size: 5, offset: 1 }} lg={{ size: 3, offset: 1 }}>
                        <PersonForm
                            onSubmit={(e) => this.onSubmit(e)}
                            onInputChange={(e) => this.onInputChange(e)}
                            form={this.state.form}
                            isEditing={
                                this.state.editingPersonTableIdx != null && 
                                this.state.editingPersonIdx != null
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col sm={{ size: 10, offset: 1 }}>
                        {this.state.personTables.map((table: PersonTableData, table_idx) => {
                            return (
                                <div key={table_idx}>
                                    <PersonTableActions
                                        table_idx={table_idx}
                                        onCopyTableClick={
                                            (table_idx) => this.onCopyTableClick(table_idx)
                                        }
                                        onDeleteTableClick={
                                            (table_idx) => this.onDeleteTableClick(table_idx)
                                        }
                                    />

                                    <PersonTable
                                        table_idx={table_idx}
                                        table={table}
                                        onEditPersonClick={
                                            (table_idx, person_idx) => this.onEditPersonClick(
                                                table_idx,
                                                person_idx
                                            )
                                        }
                                        onDeletePersonClick={
                                            (table_idx, person_idx) => this.onDeletePersonClick(
                                                table_idx,
                                                person_idx
                                            )
                                        }
                                    />
                                </div>
                            )
                        })}
                    </Col>
                </Row>
            </Container>
        )
    }
}