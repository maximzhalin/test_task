import * as React from 'react'
import {Form, FormGroup, Input, FormFeedback, Button} from 'reactstrap'
import {PersonFormData} from './entities/PersonFormData'

interface PersonFormProps {
    onSubmit: (e) => void
    onInputChange: (e) => void
    form: PersonFormData
    isEditing: boolean
}

export class PersonForm extends React.Component<PersonFormProps> {
    render () {
        return (
            <Form className="form main-container__form" onSubmit={(e) => this.props.onSubmit(e)}>
                <FormGroup className="form__form-group">
                    <Input 
                        className="form__input"
                        type="text"
                        name="name"
                        placeholder="Name"
                        invalid={this.props.form.errors.name ? true : false}
                        value={this.props.form.name}
                        onChange={(e) => this.props.onInputChange(e)}
                        onFocus={(e) => e.target.placeholder = ""} 
                        onBlur={(e) => e.target.placeholder = "Name"}
                    />
                    <FormFeedback>{this.props.form.errors.name}</FormFeedback>
                </FormGroup>
                <FormGroup className="form__form-group">
                    <Input 
                        className="form__input"
                        type="text"
                        name="surname"
                        placeholder="Surname"
                        invalid={this.props.form.errors.surname ? true : false}
                        value={this.props.form.surname}
                        onChange={(e) => this.props.onInputChange(e)}
                        onFocus={(e) => e.target.placeholder = ""} 
                        onBlur={(e) => e.target.placeholder = "Surname"}
                    />
                    <FormFeedback>{this.props.form.errors.surname}</FormFeedback>
                </FormGroup>
                <FormGroup className="form__form-group">
                    <Input 
                        className="form__input"
                        type="text"
                        name="age"
                        placeholder="Age"
                        invalid={this.props.form.errors.age ? true : false}
                        value={this.props.form.age}
                        onChange={(e) => this.props.onInputChange(e)}
                        onFocus={(e) => e.target.placeholder = ""} 
                        onBlur={(e) => e.target.placeholder = "Age"}
                    />
                    <FormFeedback>{this.props.form.errors.age}</FormFeedback>
                </FormGroup>
                <FormGroup className="form__form-group">
                    <Input 
                        className="form__input"
                        type="text"
                        name="city"
                        placeholder="City"
                        invalid={this.props.form.errors.city ? true : false}
                        value={this.props.form.city}
                        onChange={(e) => this.props.onInputChange(e)}
                        onFocus={(e) => e.target.placeholder = ""} 
                        onBlur={(e) => e.target.placeholder = "City"}
                    />
                    <FormFeedback>{this.props.form.errors.city}</FormFeedback>
                </FormGroup>
                <Button className="form__button" block>
                    {this.props.isEditing ? "Edit" : "Add" }
                </Button>
            </Form>
        )
    }
}