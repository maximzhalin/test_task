import * as React from 'react'
import {Table, Button} from 'reactstrap'
import {PersonTableData} from './entities/PersonTableData'

interface PersonTableProps {
    table_idx: number
    table: PersonTableData
    onEditPersonClick: (table_idx: number, person_idx: number) => void
    onDeletePersonClick: (table_idx: number, person_idx: number) => void
}

export class PersonTable extends React.Component<PersonTableProps> {
    getEmptyRows(persCount: number) {
        let emptyRows = []

        if (persCount < 8) {
            let emptyRowCount = 8 - persCount

            for (let i = 0; i < emptyRowCount; i++) {
                emptyRows.push(
                    <tr className="table__data-row" key={i}>
                        <td className="table__data-cell"></td>
                        <td className="table__data-cell"></td>
                        <td className="table__data-cell"></td>
                        <td className="table__data-cell"></td>
                        <td className="table__data-cell"></td>
                    </tr>
                )
            }
        }

        return emptyRows
    }

    render() {
        return (
            <Table className="table main-container__table" bordered>
                <thead className="table__head">
                    <tr>
                        <th className="table__header-cell">Name</th>
                        <th className="table__header-cell">Surname</th>
                        <th className="table__header-cell">Age</th>
                        <th className="table__header-cell">City</th>
                        <th className="table__header-cell"></th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.table.persons ? this.props.table.persons.map((person, person_idx) => {
                        return (
                            <tr className="table__data-row" key={person_idx}>
                                <td className="table__data-cell">{person.name}</td>
                                <td className="table__data-cell">{person.surname}</td>
                                <td className="table__data-cell">{person.age}</td>
                                <td className="table__data-cell">{person.city}</td>
                                <td className="table__data-cell">
                                    <Button 
                                        className="active-link-button table__one-of-two-buttons"
                                        onClick={() => this.props.onEditPersonClick(
                                            this.props.table_idx,
                                            person_idx
                                        )}
                                    >
                                        Edit
                                    </Button>
                                    
                                    <Button 
                                        className="
                                            active-link-button
                                            active-link-button_color_red 
                                            table__one-of-two-buttons"
                                        onClick={() => this.props.onDeletePersonClick(
                                            this.props.table_idx,
                                            person_idx
                                        )}
                                    >
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        )
                    }) : null}

                    {this.getEmptyRows(this.props.table.persons.length)}
                    
                </tbody>
            </Table>
        )
    }
}