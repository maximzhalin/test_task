import * as React from 'react'
import {Button} from 'reactstrap'

interface PersonTableActionsProps {
    table_idx: number
    onCopyTableClick: (table_idx: number) => void
    onDeleteTableClick: (table_idx: number) => void
}

export class PersonTableActions extends React.Component<PersonTableActionsProps> {
    render() {
        return (
            <div className="table-actions main-container__table-actions">
                <Button 
                    className="table-actions__copy-action"
                    onClick={() => this.props.onCopyTableClick(this.props.table_idx)}
                >
                    Copy table
                </Button>

                {this.props.table_idx ?
                    <Button
                        className="table-actions__delete-action"
                        onClick={() => this.props.onDeleteTableClick(this.props.table_idx)}
                    >
                        <img src="/btn_delete.svg" />
                    </Button>
                    :
                    null
                }
            </div>
        )
    }
}