export class Person {
    name: string = ""
    surname: string = ""
    age: string = ""
    city: string = ""

    constructor(data) {
        if (data) {
            this.name = data['name']
            this.surname = data['surname']
            this.age = data['age']
            this.city = data['city']
        }
    }
}