import {requiredFieldValidation, ageValidation, textValidation} from '../validations/PersonFormValidations'

interface ErrorsType {
    name: string
    surname: string
    age: string
    city: string
}

export class PersonFormData {
    name: string = ""
    surname: string = ""
    age: string = ""
    city: string = ""
    errors: ErrorsType = {
        name: "",
        surname: "",
        age: "",
        city: ""
    }

    constructor(data) {
        if (data) {
            this.name = data['name']
            this.surname = data['surname']
            this.age = data['age']
            this.city = data['city']
        }
    }

    clearFields() {
        this.name = ""
        this.surname = ""
        this.age = ""
        this.city = ""
    }

    validate() {
        let validationRes
        let hasFormErrors = false
        

        for (let [key] of Object.entries(this)) {  
            if (key != 'errors') {
                validationRes = requiredFieldValidation(this[key])
    
                if (validationRes) {
                    this.errors[key] = validationRes
                    hasFormErrors = true
                } else {
                    this.errors[key] = ""
                }
            }

            switch (key) {
                case 'name':
                    validationRes = textValidation(this[key])
                    break;
                case 'surname':
                    validationRes = textValidation(this[key])
                    break;
                case 'age':
                    validationRes = ageValidation(this[key])
                    break;
                case 'city':
                    validationRes = textValidation(this[key])
                    break;
            }

            if (validationRes && !this.errors[key]) {
                this.errors[key] = validationRes
                hasFormErrors = true
            }
        }
        
        return !hasFormErrors
    }
}