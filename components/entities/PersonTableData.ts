import {Person} from './Person'

export class PersonTableData {
    persons: Person[] = []

    constructor(data) {
        if (data) {
            this.persons = (data['persons'] as object[]).map(p => new Person(p))
        }
    }
}