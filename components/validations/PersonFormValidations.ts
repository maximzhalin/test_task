export function requiredFieldValidation(value) {
    if (value.trim() === '') {
        return `This field is required`
    }
    return null
}

export function ageValidation(value) {
    let numberRegex = /^[0-9]+$/
    if (!numberRegex.test(value)) {
        return `This field must contain only numbers`
    }
}

export function textValidation(value) {
    let textRegex = /^[a-z A-Z]+$/
    if (!textRegex.test(value)) {
        return `This field must contain only letters`
    }
}