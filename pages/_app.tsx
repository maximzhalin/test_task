import * as React from 'react'
import App, { AppProps } from 'next/app'

import '../node_modules/bootstrap/scss/bootstrap.scss'

import '../styles/main-container/main-container.scss'
import '../styles/main-container/__form/__form.scss'
import '../styles/main-container/__table-actions/__table-actions.scss'
import '../styles/main-container/__table/__table.scss'

import '../styles/form/form.scss'
import '../styles/form/__form-group/__form-group.scss'
import '../styles/form/__input/__input.scss'
import '../styles/form/__button/__button.scss'

import '../styles/table-actions/table-actions.scss'
import '../styles/table-actions/__copy-action/__copy-action.scss'
import '../styles/table-actions/__delete-action/__delete-action.scss'

import '../styles/table/table.scss'
import '../styles/table/__head/__head.scss'
import '../styles/table/__header-cell/__header-cell.scss'
import '../styles/table/__data-cell/__data-cell.scss'
import '../styles/table/__data-row/__data-row.scss'
import '../styles/table/__one-of-two-buttons/__one-of-two-buttons.scss'

import '../styles/active-link-button/active-link-button.scss'
import '../styles/active-link-button/_color/_color_red.scss'

export default class MyApp extends App<AppProps> {
  render() {
    const { Component, pageProps } = this.props

    return <Component {...pageProps} />
  }
}
