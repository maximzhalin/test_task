import Head from 'next/head'
import {HomePage} from '../components/HomePage'

export default function Home() {
    return (
        <div className='main-container'>
            <Head>
                <title>ITLABS22 test task</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <HomePage/>
        </div>
    )
}
